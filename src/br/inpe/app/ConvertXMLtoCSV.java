package br.inpe.app;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.inpe.input.ReadXML;
import br.inpe.output.GenerateCSV;

public class ConvertXMLtoCSV {

	public static void main(String[] args) {
	
		String[] metricNamesArray = {"AED", "AA", "LOC", "AC", "AMR", "NOM", "AARE", 
				"LOCAD", "ACR", "ASC", "ARC", "ARRC", "ANL", "ARP"};
		List<Integer> metricValues = new ArrayList<Integer>();
		String cwdPath = System.getProperty("user.dir");
		//int numXMLFiles = new File(cwdPath + "/xml_input/").listFiles().length; //Get the number of XML files containg the metrics
		String[] projectFileNames = new File(cwdPath + "/xml_input/").list();
		
		for(int i = 0; i < projectFileNames.length; i++)
			System.out.println(projectFileNames[i]);
		
		ReadXML inputXML = new ReadXML();
		GenerateCSV csv = new GenerateCSV();
		
		for(int i = 0; i < projectFileNames.length; i++){
			if(projectFileNames[i].contains(".xml")){
				inputXML.setFileName(cwdPath + "/xml_input/" + projectFileNames[i]);
				inputXML.loadXML();
				for(int j = 0; j < metricNamesArray.length; j++){
					metricValues.addAll(inputXML.getMetricsValuesArray(metricNamesArray[j]));
					//ordenando as metricas antes de ser inserida no csv
					Collections.sort(metricValues);
					csv.writeCSV(projectFileNames[i].replace(".xml", ""), projectFileNames[i].replace(".xml", "") + "_" + metricNamesArray[j] + ".csv", cwdPath + "/csv_output/", metricValues);
					metricValues.clear();
				}
			}
		}
		System.out.println("Finished");
	}

}
