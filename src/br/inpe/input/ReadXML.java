package br.inpe.input;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ReadXML {
		
	private String fileName;
	File inputXML; 
	NodeList nList, nProjectList, nClassList, nParentList, nTagList;
	Node node, nElementNode, nClassNode, nMetricNode, nTagNode, nParentNode;
	Document doc;
	String[] metricNamesArray = {"AED", "AA", "LOC", "AC", "AMR", "NOM", "AARE", 
			"LOCAD", "ACR", "ASC", "ARC", "ARRC", "ANL", "ARP"};
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public Document loadXML(){
		
		inputXML = new File(fileName); 
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(inputXML);
			doc.getDocumentElement().normalize();
			
			return doc;
			//return "Root Element :" + doc.getDocumentElement().getNodeName();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e){
			
		}
		return null;
	}
	
	public String getElementName(String tagName, int tagPosition) {
		
		nElementNode = getNode(tagName, tagPosition);
		return getElementName(nElementNode);
			
	}
	
	public int getNumberOfTags(String tagName, String parentTagName) {
		
		nParentNode = getNode(parentTagName, 0);
		nTagList = getListByTagName(tagName, nParentNode);
		
		return nTagList.getLength();
	}
	
	public int getNumberOfTags(String tagName) {
		
		nList = getNodeList(tagName);
		return nList.getLength();
	}
	
	public Set<String> getAttributeValues(String tagName, String attribute) {
		
		Set<String> attValues = new LinkedHashSet<String>();
		
		//Retrieve node list with the given tagname
		nList = getNodeList(tagName);
		//Search that list for all the values of that attribute
		for(int i = 0; i < nList.getLength(); i ++){
			node = nList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element ele = (Element) node;
				attValues.add(ele.getAttribute(attribute));
			}
		}
		return attValues;
	}
	
	public String getContentValue(String tagName, String tagAttribute, String attValue) {
		// Retrieves a node list of the specified tagName
		nList = getNodeList(tagName);
		
		//Find inside the list which attribute named tagAttribute has value attValue
		for(int i = 0; i < nList.getLength(); i++){
			node = nList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element ele = (Element) node;
				if(ele.getAttribute(tagAttribute).equals(attValue))
					return ele.getTextContent();
			}
		}
		
		return "Element not found or has no content";
	}
	
	public List<String> getMetricsNames() {

		Element metricElement = null;
		nClassNode = getNode("class", 0);
		Element classElement = (Element) nClassNode;
		NodeList nMetricsList = classElement.getElementsByTagName("metric");
		List<String> metricsNames = new ArrayList<String>();
		
		for(int i = 0; i < nMetricsList.getLength(); i++){
			nMetricNode = nMetricsList.item(i);
			if(nMetricNode.getNodeType() == Node.ELEMENT_NODE)
				metricElement = (Element) nMetricNode;
			metricsNames.add(metricElement.getAttribute("alias"));
		}
		
		return metricsNames;
	}
	
	public Map<String, String> getMetricsValues(String className, String metricName) {
		
		Map<String, String> metricValues = new HashMap<>();
		
		Element classElement = getElementByName("class", className);
		NodeList nMetricsList = classElement.getElementsByTagName("metric");
		Element metricElement = getElementByName(metricName, nMetricsList);
		NodeList nSingleMetricValue = metricElement.getElementsByTagName("element");
		if(nSingleMetricValue.getLength() != 0)
			for(int i = 0; i < nSingleMetricValue.getLength(); i++){
				Node metric = nSingleMetricValue.item(i);
				if(metric.getNodeType() == Node.ELEMENT_NODE){
					Element singleMetricElement = (Element) metric;
					metricValues.put(singleMetricElement.getAttribute("name"), singleMetricElement.getTextContent());
				}
			}
		else{
			metricValues.put(metricElement.getAttribute("alias"), metricElement.getTextContent());
		}
		return metricValues;
	}
	
	public List<Integer> getMetricsValuesArray(String metrics) {
		
		List<Integer> metricValues = new ArrayList<Integer>();
		NodeList nMetricsList, nEleList;
		Node nMetricNode, nEleNode;
		Element metricElement, eleElement;
		//Fetches all metrics in XML document
		nMetricsList = getNodeList("metric");
		//System.out.println("project name:" + getElementName("java-project", 0));
		System.out.println();
		//Loops through all metrics with a given name
		for(int i = 0; i < nMetricsList.getLength(); i++){
			nMetricNode = nMetricsList.item(i);
			if(nMetricNode.getNodeType() == Node.ELEMENT_NODE){
				metricElement = (Element) nMetricNode;
				if(metricElement.getAttribute("alias").equals(metrics)){
					nEleList = metricElement.getElementsByTagName("element");
					//If is a class metric, fetch the value directly. Otherwise loop through the properties
					if(nEleList.getLength() == 0){//Class metric
						System.out.println("name: " + metricElement.getTextContent());
						metricValues.add(Integer.parseInt(metricElement.getTextContent()));
					}else //Loop through all elements inside <metrics>
						for(int j = 0; j < nEleList.getLength(); j++){
							nEleNode = nEleList.item(j);
							if(nEleNode.getNodeType() == Node.ELEMENT_NODE){
								eleElement = (Element) nEleNode;
								metricValues.add(Integer.parseInt(eleElement.getTextContent()));
							}
						}
				}
			}
		}
		
		return metricValues;
		
	}
	
	//Inner helper methods
	private String getElementName(Node elementNode) {

		if(elementNode.getNodeType() == Node.ELEMENT_NODE){
			Element element = (Element) elementNode;
			return element.getAttribute("name");
		}
		return "No element name";
	}
	
	private Node getNode(String tagName, int elementPosition){
		
		nList = doc.getElementsByTagName(tagName);
		node = nList.item(elementPosition);
		return node;
	}
	
	private NodeList getListByTagName(String tagName, Node parentNode) {
		
		if(parentNode.getNodeType() == Node.ELEMENT_NODE){
			Element ele = (Element) parentNode;
			nList = getNodeList("metric", ele);
			return nList;
		}
			
		return null;
	}
	
	private NodeList getNodeList(String tagName) {
		
		nList = doc.getElementsByTagName(tagName);
		// TODO Auto-generated method stub
		return nList;
	}
	
	private NodeList getNodeList(String tagName, Element element) {
		
		nList = element.getElementsByTagName(tagName);
		// TODO Auto-generated method stub
		return nList;
	}
	
		
	private Element getElementByName(String tagName, String className){
		nList = doc.getElementsByTagName(tagName);
		for(int i = 0; i < nList.getLength(); i++){
			node = nList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element element = (Element) node;
				if(element.getAttribute("name").equals(className))
					return element;
			}
		}
		return null;
	}
	
	private Element getElementByName(String tagName, NodeList nList){
		for(int i = 0; i < nList.getLength(); i++){
			node = nList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element element = (Element) node;
				if(element.getAttribute("alias").equals(tagName))
					return element;
			}
		}
		return null;
	}
	
}
