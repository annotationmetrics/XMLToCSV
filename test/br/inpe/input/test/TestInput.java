package br.inpe.input.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import br.inpe.input.ReadXML;

public class TestInput {

	String fileName;
	ReadXML inputXML;
	List<String> tagNames;
	String[] metricNamesArray = {"AED", "AA", "LOC", "AC", "AMR", "NOM", "AARE", 
						"LOCAD", "ACR", "ASC", "ARC", "ARRC", "ANL", "ARP"};
	String[] metricPropertyNames = {"Priority", "value"};
	int[] metricValuesPerElement = {2,0};
	
	@Before
	public void initialize(){

		String cwdPath = System.getProperty("user.dir");
		fileName = cwdPath + "/xml_input/metrics_TomCat.xml";
		inputXML = new ReadXML();
		inputXML.setFileName(fileName);
		inputXML.loadXML();
		tagNames = new ArrayList<String>();
	}

	@Test
	public void readProjectName(){
		
		String projectName = inputXML.getElementName("java-project", 0);
		assertEquals("tomcat-TOMCAT_9_0_0_M11", projectName);
	}
	
	@Test
	public void readClassNames(){
		
		String firstClass, secondClass, thirdClass;
		firstClass = inputXML.getElementName("class", 0);
		secondClass = inputXML.getElementName("class", 1);
		thirdClass = inputXML.getElementName("class", 2);
		
		assertEquals("Priority.java", firstClass);
		assertEquals("Resources.java", secondClass);
		assertEquals("Resource.java", thirdClass);
	}
	
	@Test
	public void getMetricsNumberPerClass(){
		
		int numMetrics = inputXML.getNumberOfTags("metric", "class");
		assertEquals(14, numMetrics);
	}
	
	@Test
	public void getMetricNumberByTag(){
		int numMetric = inputXML.getNumberOfTags("metric");
		assertEquals(20524, numMetric);
	}
	
	@Test
	public void getAttributeValue(){
		
		Set<String> values = new LinkedHashSet<String>();
		values = inputXML.getAttributeValues("metric", "alias");
		assertTrue(values.contains("LOCAD"));
	}
	
	@Test
	public void getContentValues(){
		
		//String value = inputXML.getContentValue("element", "name", "DataSourceDefinitions");
		//assertEquals("2", value);
		String value = inputXML.getContentValue("metric", "alias", "LOC");
		assertEquals("10", value);
	}
	
	@Test
	public void printMetricsName(){
		
		List<String> metricsNames = new ArrayList<String>();
		metricsNames = inputXML.getMetricsNames();
		for(int i = 0; i < metricsNames.size(); i++)
			assertEquals(metricNamesArray[i], metricsNames.get(i));
	}
	
	@Test
	public void printMetricsElement(){
		
		Map<String, String> metricsValues = new HashMap<>();
		metricsValues = inputXML.getMetricsValues("ManagedBean.java", "LOC");
		assertEquals("10", metricsValues.get("LOC"));
		
	}
	
	@Test
	public void printMetricsValues(){
		List<Integer> metricValues = new ArrayList<Integer>();
		metricValues = inputXML.getMetricsValuesArray("AED");
		assertEquals(new Integer(2), metricValues.get(0));
	}

}
